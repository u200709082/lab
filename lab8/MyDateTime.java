public class MyDateTime extends Object{

    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public String toString(){
        return date+" "+time;
    }

    public void incrementDay(){
        date.incrementDay();
    }

    public void incrementHour(){
        incrementHour(1);
    }

    public void incrementHour(int diff){
        int dayDiff = time.incrementHour(diff);
        if (dayDiff < 0 )
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);

    }

    public void decrementHour(int diff){
        incrementHour(-diff);
    }

    public void incrementMinute(int diff){
        int dayDiff = time.incrementMinute(diff);
        if (dayDiff < 0 )
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);
    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int diff){
        date.incrementYear(diff);
    }
    public void decrementDay(){
        date.decrementDay();
    }
    public void decrementYear(){
        date.decrementYear();
    }
    public void decrementMonth(){
        date.decrementMonth();
    }
    public void incrementDay(int diff){
        date.incrementDay(diff);
    }
    public void decrementMonth(int diff){
        date.decrementMonth(diff);
    }
    public void decrementDay(int diff){
        date.decrementDay(diff);
    }
    public void incrementMonth(int diff){
        date.incrementMonth(diff);
    }
    public void decrementYear(int diff){
        date.decrementYear(diff);
    }
    public void incrementMonth(){
        date.incrementMonth();
    }
    public void incrementYear(){
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime){
        if(anotherDateTime.date.year > date.year)
            return true;
        else if (anotherDateTime.date.year < date.year)
            return false;
        else

            if(anotherDateTime.date.month > date.month)
                return true;
            else if (anotherDateTime.date.month < date.month)
                return false;
            else

                if(anotherDateTime.date.day > date.day)
                    return true;
                else if (anotherDateTime.date.day < date.day)
                    return false;
                else

                    if(anotherDateTime.time.hour > time.hour)
                        return true;
                    else if (anotherDateTime.time.hour < time.hour)
                        return false;
                    else

                        if(anotherDateTime.time.minute > time.minute)
                            return true;
                        else if (anotherDateTime.time.minute < time.minute)
                            return false;
                        else
                            return false;
    }

    public boolean isAfter(MyDateTime anotherDateTime){
        if(anotherDateTime.date.year < date.year)
            return true;
        else if (anotherDateTime.date.year > date.year)
            return false;
        else

            if(anotherDateTime.date.month < date.month)
                return true;
            else if (anotherDateTime.date.month > date.month)
                return false;
            else

                if(anotherDateTime.date.day < date.day)
                    return true;
                else if (anotherDateTime.date.day > date.day)
                    return false;
                else

                    if(anotherDateTime.time.hour < time.hour)
                        return true;
                    else if (anotherDateTime.time.hour > time.hour)
                        return false;
                    else

                        if(anotherDateTime.time.minute < time.minute)
                            return true;
                        else if (anotherDateTime.time.minute > time.minute)
                            return false;
                        else
                            return false;
    }

    public String dayTimeDifference(MyDateTime anotherDateTime){
        int diff = 0;
        int minuteDiff = 0;
        int hourDiff = 0;
        int dayDiff = 0;

        if (isBefore(anotherDateTime)){
            while (isBefore(anotherDateTime)){
                diff++;
                anotherDateTime.decrementMinute(1);
            }
            dayDiff = diff / (24*60);
            hourDiff = (diff-(dayDiff*24*60)) / 60;
            minuteDiff = diff - ((hourDiff*60)+(dayDiff*24*60));
            anotherDateTime.incrementMinute(diff);
        }

        else if (isAfter(anotherDateTime)){
            while (isAfter(anotherDateTime)){
                diff++;
                anotherDateTime.incrementMinute(1);
            }
            dayDiff = diff / (24*60);
            hourDiff = (diff-(dayDiff*24*60)) / 60;
            minuteDiff = diff - ((hourDiff*60)+(dayDiff*24*60));
            anotherDateTime.decrementMinute(diff);
        }
        if (minuteDiff!=0 && hourDiff!=0 && dayDiff!=0)
            return dayDiff+" day(s) "+hourDiff+" hour(s) "+minuteDiff+" minute(s)";
        else if (hourDiff!=0 && dayDiff!=0)
            return dayDiff+" day(s) "+hourDiff+" hour(s)";
        else if (dayDiff!=0)
            return dayDiff+" day(s)";
        else
            return hourDiff+" hour(s)";
    }
}
