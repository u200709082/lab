public class GCDRecursion {
    public static void main(String[] args) {
        System.out.println("GCD is : "+gcd(1071,462));
    }
    public static int gcd(int a , int b){
        int r = a%b;
        if (r!=0)
            return gcd(b,r);
        else
            return b;
    }
}
