public class Rectangle {
    
    public Point topLeft;
    private int widht;
    private int height;

    public Rectangle(Point topLeft, int widht, int height){

        this.topLeft = topLeft;
        this.widht = widht;
        this.height = height;


    }

    public int area(){
        return widht * height;
    }

    public int perimeter(){
        return 2 * (widht + height);
    }

    public Point[] corners(){
        Point[] corners = new Point [4];
        corners[0] = topLeft;
        corners[1] = new Point(topLeft.getxCoord()+ widht, topLeft.getyCoord());
        corners[2] = new Point(topLeft.getxCoord(), topLeft.getyCoord() -height);
        corners[3] = new Point(topLeft.getxCoord() + widht, topLeft.getyCoord()- height);

        return corners;
    }

}