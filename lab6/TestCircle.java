public class TestCircle{

    public static void main(String[] args){
        Circle c = new Circle( 8 , new Point(15,12));

        System.out.println("Area of circle is " + c.area());
        System.out.println("Perimeter of the circle is " + c.perimeter());
    }
}