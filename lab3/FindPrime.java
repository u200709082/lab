import java.io.IOException;
import java.util.Scanner;

public class FindPrime {
    public static void main(String[] args) {
        int max = Integer.parseInt(args[0]);

        for (int number=2;number<max;number++) {
            int divisor = 2;
            boolean isPrime = true;
            while (divisor < number) {
                if (number % divisor == 0) {
                    isPrime = false;
                }
                divisor++;
            }
            if (isPrime)
                System.out.print(number+" ");

        }



    }
}
