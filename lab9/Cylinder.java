package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    int height;

    public Cylinder(double radius, int height) {
        super(radius);
        this.height = height;
    }
    @Override
    public String toString(){
        return "radius = "+radius + ", pi = "+Math.PI + ", height = "+height;
    }

    public double area(){
        return (2*super.area() + height*(2*Math.PI*radius));
    }
    public double volume() {
        return height * super.area();
    }
}

