package shapes3d;

import shapes2d.Square;

public class Cube extends Square{
    public double height;

    public Cube(double length , double width ,double height){
        super(length, width);
        this.height = height;
    }

    public double area(){
        return 6 * super.area();
    }
    public double volume(){
        return super.area()*height;
    }
    public String toString(){
        return "length = "+length + ", width = "+width + ", height = "+height;
    }
}
