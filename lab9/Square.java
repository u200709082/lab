package shapes2d;


public class Square {
    public double length;
    public double width;

    public Square(double length, double width){
        this.length = length;
        this.width = width;
    }

    public String toString(){
        return "length = "+ length + " width = "+width;
    }
    public double area(){
        return length * width;
    }
}
