import shapes3d.Cube;
import shapes3d.Cylinder;

public class TestShapes {
    public static void main(String[] args) {
        Cylinder cylinder = new Cylinder(2.0,3,4);
        Cube cube = new Cube(4,5,10);

        String strRepCylinder = cylinder.toString();
        System.out.println(strRepCylinder);
        System.out.println("Area = "+cylinder.area());
        System.out.println("Volume = "+cylinder.volume());

        System.out.println();

        String strRepCube = cube.toString();
        System.out.println(strRepCube);
        System.out.println("Area = "+cube.area());
        System.out.println("Volume = "+cube.volume());

    }
}
