package shapes2d;

public class Circle {
    public double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public String toString(){
        return "radius = "+radius + " pi = "+Math.PI;
    }
    public double area(){
        return Math.PI * Math.pow(radius,2);
    }
}
