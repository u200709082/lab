public class FindMaximum {

	public static void main(String[] args){
        int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int result;

        boolean someCondition = value1 > value2;

        result = value1 > value2 ? value1 : value2; //if value1 will be result, if false value2 will be result

        System.out.println(result);

	}
}